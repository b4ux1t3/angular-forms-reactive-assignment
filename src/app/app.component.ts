import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Words } from './words';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  wordList: Words;
  projectForm: FormGroup;
  diceRoll: number;

  ngOnInit(): void {
    this.wordList = new Words();
    this.initializeForm();
    this.setDiceNum();
  }

  initializeForm(): void {
    const randomAdjective = this.wordList.getAdjective();
    const randomNoun = this.wordList.getNoun();

    this.projectForm = new FormGroup({
      'projectName': new FormControl(`${randomAdjective} ${randomNoun}`, [Validators.required], [this.validateName.bind(this)]),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'status': new FormControl('Stable'),
    });
  }

  validateName(control: FormControl): Promise<{[s: string]: boolean}> | Observable<{[s: string]: boolean}>{
    const promise = new Promise<{[s: string]: boolean}>((resolve =>{
      setTimeout(() => {
        if (control.value === 'Test')
        {
          resolve({'nameIsForbidden': true});
        } else {
          resolve(null);
        }
      });
    }));

    return promise;
  }

  onSubmit(){
    console.log(this.projectForm.value);
    this.projectForm.reset();
    this.genName();
  }

  genName(){
    const randomAdjective = this.wordList.getAdjective();
    const randomNoun = this.wordList.getNoun();
    this.projectForm.get('projectName').setValue(`${randomAdjective} ${randomNoun}`);
    this.setDiceNum();
  }

  setDiceNum(){
    this.diceRoll = Math.floor(Math.random() * 6) + 1;
  }
}
